package com.tgfc.tw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication(scanBasePackages = "com.tgfc.tw")
@EntityScan(basePackages={"com.tgfc.tw.model"})
public class SpringTestApplication extends SpringBootServletInitializer {

    public static void main(String arg[]){
        SpringApplication.run(SpringTestApplication.class);
    }
}
