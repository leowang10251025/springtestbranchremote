package com.tgfc.tw.repository;

import com.tgfc.tw.model.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolePageRepository extends JpaRepository<Role, Integer> {
    Page<Role> findAllByFirstNameLike(String firstName, Pageable pageable);
    Page<Role> findAllByFirstNameAndLastName(String firstName, String lastName, Pageable pageable);
    Page<Role> findAllBy(Pageable pageable);
}
