package com.tgfc.tw.repository;

import com.tgfc.tw.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role getById(Integer id);
}
