package com.tgfc.tw.controller;

import com.tgfc.tw.model.Role;
import com.tgfc.tw.repository.RolePageRepository;
import com.tgfc.tw.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RoleController {
    @GetMapping(value="/role/task")
    public String task(){
        return "Hello World!";
    }

    @Autowired
    RoleRepository roleRepository;
    @Autowired
    RolePageRepository rolePageRepository;

    @GetMapping(value = "/role/task1")
    public String task1(){
        Role role =roleRepository.getById(3);
        return role.getId()+", "+role.getFirstName();
    }

    @GetMapping(value = "/role/task2")
    public List<Role> task2(){
        List<Role> roles =rolePageRepository.findAll();
        return roles;
    }

    @GetMapping(value = "/role/task3")
    public Page<Role> task3(@RequestParam String Keyword){
        Pageable pageable = PageRequest.of(0,5);
        Page<Role> rolesByName =rolePageRepository.findAllByFirstNameLike("%"+Keyword+"%", pageable);
        return rolesByName;
    }

    @GetMapping(value="/role/task4")
    public Page<Role> task4(@RequestParam int pageIndex, @RequestParam int pageNumber, @RequestParam String condition1, @RequestParam String condition2){
        Pageable pageable = PageRequest.of(pageIndex,pageNumber);
        Page<Role> rolesByFirstLastName = rolePageRepository.findAllBy(pageable);
//        Page<Role> rolesByFirstLastName=rolePageRepository.findAllByFirstNameAndLastName(condition1, condition2,pageable);
        return rolesByFirstLastName;
    }
}
