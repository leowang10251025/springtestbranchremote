package com.tgfc.tw.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TriangleAccController {
    @GetMapping(value="/math/excise1")
    public String getResult(@RequestParam int sideA, @RequestParam int sideB){
        String test1="test1";
        float test2=3.0f;
        double result3=Math.pow(3,2);
        double result= this.accResult((double)sideA, (double)sideB);
        return "A邊長 : "+sideA+"，B邊長 : "+sideB+", 結果為:"+result;
    }

    private double accResult(double sideA, double sideB){
        return Math.sqrt(Math.pow(sideA,2)+Math.pow(sideB,2));
    }
}
