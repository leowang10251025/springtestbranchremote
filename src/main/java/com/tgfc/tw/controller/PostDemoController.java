package com.tgfc.tw.controller;

import com.tgfc.tw.model.PostDemo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostDemoController {
    @PostMapping("demo/test")
    public String Test(){
        return "Hello World!";
    }

    @PostMapping("demo/postDemo")
    public String postDemo(@RequestBody PostDemo request){
        return request.toString();
    }
}
